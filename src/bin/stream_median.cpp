#include "src/lib/median.hpp"
#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>

int main(int argc, char* argv[])
{
  if (argc < 3)
  {
    std::cerr << "at least two arguments expected..." << std::endl;
    return 1;
  }
  try
  {
    argc -= 2; // binary name and first argument
    ++argv;
    cxx::median median(std::stoi(*argv));
    std::cout.setf(std::ios::fixed, std::ios::floatfield);
    while (argc-- > 1)
    {
      ++argv;
      if (std::strcmp("q", *argv) == 0) return 0;
      if (std::strcmp("m", *argv) == 0)
        std::cout << std::setprecision(1) << median() << ' ';
      else
        median.push(std::stoi(*argv));
    }
  } catch (std::invalid_argument const& e)
  {
    std::cerr << "error: " << e.what() << std::endl;
    return 1;
  } catch (std::out_of_range const& e)
  {
    std::cerr << "error: " << e.what() << std::endl;
    return 1;
  }

  std::cout << std::endl;
  return 0;
}
