#include <cxx/utility.hpp>
#include <cxx/dynamic_array.hpp>
#include <cxx/heap.hpp>
namespace cxx
{
  template <class T, class Compare = cxx::less<>, class Allocator = cxx::allocator<T>>
  class heap {
    using storage_t = cxx::dynamic_array<T, Allocator>;
    struct impl_t : Compare {
      storage_t storage;
    } impl;

  public:
    using size_type = typename storage_t::size_type;
    using const_iterator = typename storage_t::const_iterator;

    explicit heap(size_type capa = 128) { impl.storage.reserve(capa); }

    [[nodiscard]] T const& front() const noexcept { return impl.storage.front(); }

    T const& push(T t)
    {
      impl.storage.emplace_back(cxx::move(t));
      cxx::push_heap(impl.storage.begin(), impl.storage.end(), get_compare());
      return front();
    }

    [[nodiscard]] T pop()
    {
      cxx::pop_heap(impl.storage.begin(), impl.storage.end(), get_compare());
      T value = cxx::move(impl.storage.back());
      impl.storage.pop_back();
      return value;
    }

    Compare const& get_compare() const { return static_cast<Compare const&>(impl); }

    [[nodiscard]] size_type size() const noexcept { return impl.storage.size(); }

    [[nodiscard]] bool empty() const noexcept { return size() == 0; }

    [[nodiscard]] const_iterator begin() const noexcept { return impl.storage.begin(); }

    [[nodiscard]] const_iterator end() const noexcept { return impl.storage.end(); }
  };
} // namespace cxx
