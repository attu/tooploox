#pragma once

#include "src/lib/heap.hpp"

namespace cxx
{
  class median {
    cxx::heap<int, cxx::less<>> lows;
    cxx::heap<int, cxx::greater<>> highs;

    double calc() const;
    void rebalance();

  public:
    explicit median(int);
    double operator()() const;
    double push(int);
  };
} // namespace cxx
