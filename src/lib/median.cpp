#include "src/lib/median.hpp"

double cxx::median::calc() const
{
  if (lows.size() == highs.size()) return (double(lows.front()) + double(highs.front())) / 2;
  if (lows.size() > highs.size()) return double(lows.front());
  return double(highs.front());
}

void cxx::median::rebalance()
{
  if (lows.size() == highs.size()) return;
  if (lows.size() > highs.size() && (lows.size() - highs.size()) == 1) return;
  if (lows.size() < highs.size() && (highs.size() - lows.size()) == 1) return;
  if (lows.size() > highs.size())
    highs.push(lows.pop());
  else
    lows.push(highs.pop());
}

cxx::median::median(int first)
{
  lows.push(first);
}

double cxx::median::operator()() const
{
  return calc();
}

double cxx::median::push(int x)
{
  if (x >= lows.front())
    highs.push(x);
  else
    lows.push(x);
  rebalance();
  return calc();
}
