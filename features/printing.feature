Feature: stream_median prints median on each "m" in input

Scenario: stream_median prints only when "m" is one of args
  Given stream_median is called with "1 2 3 q"
  Then it prints " " on stdout
  And exitcode is 0

Scenario: stream_median prints on each "m" in args
  Given stream_median is called with "1 2 m 3 m q"
  Then it prints "1.5 2.0" on stdout
  And exitcode is 0
  Given stream_median is called with "3 5 m 8 m 6 m q"
  Then it prints "4.0 5.0 5.5" on stdout
  And exitcode is 0
