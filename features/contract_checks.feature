Feature: stream_median checks input contract

Scenario: stream_median expects at least 2 arguments
  Given stream_median is called with " "
  Then it prints "at least two arguments expected..." on stderr
  And exitcode is 1
  Given stream_median is called with "42"
  Then it prints "at least two arguments expected..." on stderr
  And exitcode is 1

Scenario: stream_median expects that first argument is an integer
  Given stream_median is called with "q q"
  Then it prints "error: stoi" on stderr
  And exitcode is 1
  Given stream_median is called with "m q"
  Then it prints "error: stoi" on stderr
  And exitcode is 1
  Given stream_median is called with "lorem ipsum"
  Then it prints "error: stoi" on stderr
  And exitcode is 1

Scenario: stream_median expects that integer is in int32_t range
  Given stream_median is called with "2147483648 m q"
  Then it prints "error: stoi" on stderr
  And exitcode is 1
  Given stream_median is called with "-2147483649 m q"
  Then it prints "error: stoi" on stderr
  And exitcode is 1

Scenario: calculating medain does not cause an integer overflow
  Given stream_median is called with "1 2147483647 m q"
  Then it prints "1073741824.0" on stdout
  And exitcode is 0
