Feature: stream_median works with expected input

Scenario: Three integers are passed followed by m and q
  Given stream_median is called with "1 2 3 m q"
  Then it prints "2.0" on stdout
  And exitcode is 0

Scenario: Single integer is passed followed by m and q
  Given stream_median is called with "42 m q"
  Then it prints "42.0" on stdout
  And exitcode is 0

Scenario: Negative numbers are also handled
  Given stream_median is called with "-42 -1 -2 0 m q"
  Then it prints "-1.5" on stdout
  And exitcode is 0

# Feature: stream_median checks input contract

Scenario: stream_median expects at least 2 arguments
  Given stream_median is called with " "
  Then it prints "at least two arguments expected..." on stderr
  And exitcode is 1
  Given stream_median is called with "42"
  Then it prints "at least two arguments expected..." on stderr
  And exitcode is 1

Scenario: stream_median expects that first argument is an integer
  Given stream_median is called with "q q"
  Then it prints "error: stoi" on stderr
  And exitcode is 1
  Given stream_median is called with "m q"
  Then it prints "error: stoi" on stderr
  And exitcode is 1
  Given stream_median is called with "lorem ipsum"
  Then it prints "error: stoi" on stderr
  And exitcode is 1

Scenario: stream_median expects that integer is in int32_t range
  Given stream_median is called with "2147483648 m q"
  Then it prints "error: stoi" on stderr
  And exitcode is 1
  Given stream_median is called with "-2147483649 m q"
  Then it prints "error: stoi" on stderr
  And exitcode is 1

Scenario: stream_median prints only when "m" is one of args
  Given stream_median is called with "1 2 3 q"
  Then it prints " " on stdout
  And exitcode is 0

Scenario: stream_median prints on each "m" in args
  Given stream_median is called with "1 2 m 3 m q"
  Then it prints "1.5 2.0" on stdout
  And exitcode is 0
  Given stream_median is called with "3 5 m 8 m 6 m q"
  Then it prints "4.0 5.0 5.5" on stdout
  And exitcode is 0
