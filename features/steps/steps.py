from glob import glob
import subprocess
import os


def isexe(fpath):
    return os.path.isfile(fpath) and os.access(fpath, os.X_OK)


@given(u'{name} is called with "{args}"')
def step_impl(context, name: str, args: str):
    path = next(f for f in glob(f'**/{name}', recursive=True) if isexe(f))
    context.completed = subprocess.run(
        f'{path} {args}'.split(), capture_output=True)


@then(u'it prints "{expected}" on {stream}')
def step_impl(context, expected: str, stream):
    expected = expected.strip()
    out = getattr(context.completed, stream).decode('utf-8').strip()
    assert out == expected, f'{out!r} != {expected!r}'


@then(u'exitcode is {expected:d}')
def step_impl(context, expected: int):
    exitcode = context.completed.returncode
    assert exitcode == expected, f'{exitcode} != {expected}'
