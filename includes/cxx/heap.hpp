#pragma once
#include <cxx/utility.hpp>

namespace cxx
{
  namespace detail
  {
    template <class RandomIt, class Distance, class Value, class Compare>
    inline void push_heap(RandomIt first,
                          Distance idx,
                          Distance top,
                          Value value,
                          Compare comp) noexcept
    {
      Distance parent = (idx - 1) / 2;
      while (idx > top && comp(*(first + parent), value))
      {
        *(first + idx) = cxx::move(*(first + parent));
        idx = parent;
        parent = (idx - 1) / 2;
      }
      *(first + idx) = cxx::move(value);
    }

    template <class RandomIt, class Distance, class Value, class Compare>
    inline void adjust_heap(RandomIt first,
                            Distance idx,
                            Distance len,
                            Value value,
                            Compare comp) noexcept
    {
      Distance const top = idx;
      Distance child = idx;
      while (child < (len - 1) / 2)
      {
        child = 2 * (child + 1);
        if (comp(*(first + child), *(first + (child - 1)))) child--;
        *(first + idx) = cxx::move(*(first + child));
        idx = child;
      }
      if ((len & 1) == 0 && child == (len - 2) / 2)
      {
        child = 2 * (child + 1);
        *(first + idx) = cxx::move(*(first + (child - 1)));
        idx = child - 1;
      }
      cxx::detail::push_heap(first, idx, top, cxx::move(value), comp);
    }

    template <class RandomIt, class Compare>
    inline void pop_heap(RandomIt first, RandomIt last, RandomIt result, Compare comp)
    {
      using value_type = typename cxx::iterator_traits<RandomIt>::value_type;
      using difference_type = typename cxx::iterator_traits<RandomIt>::difference_type;

      value_type value = cxx::move(*result);
      *result = cxx::move(*first);
      cxx::detail::adjust_heap(first, difference_type(0), difference_type(last - first),
                               cxx::move(value), comp);
    }
  } // namespace detail

  template <class RandomIt, class Compare>
  inline void make_heap(RandomIt first, RandomIt last, Compare comp)
  {
    using value_type = typename cxx::iterator_traits<RandomIt>::value_type;
    using difference_type = typename cxx::iterator_traits<RandomIt>::difference_type;
    difference_type const distance = last - first;
    if (distance < 2) return;

    difference_type parent_index = (distance - 2) / 2;
    while (true)
    {
      value_type value = cxx::move(*(first + parent_index));
      detail::adjust_heap(first, parent_index, distance, cxx::move(value), comp);
      if (parent_index == 0) break;
      --parent_index;
    }
  }

  template <class RandomIt, class Compare>
  inline void push_heap(RandomIt first, RandomIt last, Compare comp)
  {
    using value_type = typename cxx::iterator_traits<RandomIt>::value_type;
    using difference_type = typename cxx::iterator_traits<RandomIt>::difference_type;

    value_type value = cxx::move(*(last - 1));
    cxx::detail::push_heap(first, difference_type((last - first) - 1), difference_type(0),
                           cxx::move(value), comp);
  }

  template <class RandomIt, class Compare>
  inline void pop_heap(RandomIt first, RandomIt last, Compare comp)
  {
    if (last - first < 2) return;
    --last;
    cxx::detail::pop_heap(first, last, last, comp);
  }
} // namespace cxx
