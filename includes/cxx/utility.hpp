#pragma once

namespace cxx
{
  using size_t = unsigned long;

  namespace detail
  {
    template <class T, class U = T&&>
    U declval(int);
    template <class T>
    T declval(long);

  } // namespace detail

  template <class T>
  auto declval() noexcept -> decltype(detail::declval<T>(0));

  template <class T>
  struct remove_reference {
    using type = T;
  };

  template <class T>
  struct remove_reference<T&> {
    using type = T;
  };

  template <class T>
  struct remove_reference<T&&> {
    using type = T;
  };

  template <class T>
  using remove_reference_t = typename remove_reference<T>::type;

  template <class T>
  struct remove_const {
    using type = T;
  };

  template <class T>
  struct remove_const<T const> {
    using type = T;
  };

  template <class T>
  using remove_const_t = typename remove_const<T>::type;

  template <class Iterator>
  struct iterator_traits {
    using value_type = remove_const_t<remove_reference_t<decltype(*cxx::declval<Iterator>())>>;
    using difference_type = decltype(cxx::declval<Iterator>() - cxx::declval<Iterator>());
  };

  template <class T>
  constexpr T&& forward(remove_reference_t<T>& t) noexcept
  {
    return static_cast<T&&>(t);
  }

  template <class T>
  constexpr T&& forward(remove_reference_t<T>&& t) noexcept
  {
    return static_cast<T&&>(t);
  }

  template <class T>
  constexpr remove_reference_t<T>&& move(T&& t) noexcept
  {
    return static_cast<remove_reference_t<T>&&>(t);
  }

  template <typename T>
  void swap(T& lhs, T& rhs) noexcept
  {
    T tmp = cxx::move(lhs);
    lhs = cxx::move(rhs);
    rhs = cxx::move(tmp);
  }

  template <class T = void>
  struct less {
    template <class U, class V>
    bool operator()(U const& lhs, V const& rhs) const noexcept
    {
      return lhs < rhs;
    }
  };

  template <class T = void>
  struct greater {
    template <class U, class V>
    bool operator()(U const& lhs, V const& rhs) const noexcept
    {
      return lhs > rhs;
    }
  };
} // namespace cxx
