#pragma once
#include <cxx/utility.hpp>
#include <new>

namespace cxx
{
  template <size_t Len, size_t Align>
  struct aligned_storage {
    struct type {
      alignas(Align) unsigned char data[Len];
    };
  };

  template <class T>
  struct allocator {
    using storage_type = typename aligned_storage<sizeof(T), alignof(T)>::type;
    using pointer = T*;
    using size_type = size_t;

    [[nodiscard]] pointer allocate(size_type s) noexcept
    {
      // terminate in case of bad_alloc: stop this insane challenge of handling exhausted memory...
      auto ptr = new storage_type[s];
      return reinterpret_cast<pointer>(ptr);
    }

    void deallocate(pointer p, size_type) noexcept
    {
      auto ptr = reinterpret_cast<storage_type*>(p);
      delete[] ptr;
    }
  };

  template <class T, class Allocator = allocator<T>>
  struct dynamic_array {
    static constexpr size_t const growing_factor = 2;
    using size_type = size_t;
    using pointer = T*;
    using const_pointer = T const*;
    using iterator = pointer;
    using const_iterator = const_pointer;
    using reference = T&;
    using const_reference = T const&;

    dynamic_array() noexcept = default;

    dynamic_array(dynamic_array const& orig) : storage{orig.size()}
    {
      for (auto const& x : orig) { emplace_back(x); }
    }

    dynamic_array(dynamic_array&& orig) noexcept : storage{move(orig.storage)}
    {
      // moved-from object can be nulled
      orig.storage.first = orig.storage.last = orig.storage.end_of_storage = nullptr;
    }

    dynamic_array& operator=(dynamic_array const& orig)
    {
      // need to clear current state here as there is nothing to do with it
      clear();
      // reallocates only if needed
      reserve(orig.size());
      for (auto const& x : orig) { emplace_back(x); }
      return *this;
    }

    dynamic_array& operator=(dynamic_array&& orig) noexcept
    {
      // swap state to allow move assign to be faster
      // items deletion and memory deallocation is being postponed till orig's dtor
      swap(storage, orig.storage);
      return *this;
    }

    ~dynamic_array() noexcept
    {
      clear();
      if (capacity() != 0) { storage.deallocate(begin(), capacity()); }
    }

    [[nodiscard]] size_type size() const noexcept
    {
      return static_cast<size_type>(end() - begin());
    }

    [[nodiscard]] bool empty() const noexcept { return size() == 0; }

    [[nodiscard]] size_type capacity() const noexcept
    {
      return static_cast<size_type>(
          storage.first != storage.end_of_storage ? storage.end_of_storage - storage.first : 0);
    }

    [[nodiscard]] iterator begin() noexcept { return storage.first; }
    [[nodiscard]] const_iterator begin() const noexcept { return storage.first; }

    [[nodiscard]] iterator end() noexcept { return storage.last; }
    [[nodiscard]] const_iterator end() const noexcept { return storage.last; }

    [[nodiscard]] reference front() noexcept { return *begin(); }
    [[nodiscard]] const_reference front() const noexcept { return *begin(); }

    [[nodiscard]] reference back() noexcept { return *(end() - 1); }
    [[nodiscard]] const_reference back() const noexcept { return *(end() - 1); }

    void pop_back() noexcept
    {
      // shrink size and destroy last items
      // capacity stays unchanged
      storage.last--;
      storage.last->~T();
    }

    void reserve(size_type s)
    {
      // if we have enough memory do nothing
      if (capacity() >= s) return;
      // otherwise create a new buffer
      storage_t next{s};
      auto first = begin();
      while (first != end())
      {
        // if T's move-ctor can throw this should fallback to copying
        new ((void*)next.last) T(cxx::move(*first++));
        next.last++;
      }
      // cleanup moved-out objects
      clear();
      storage.deallocate(begin(), capacity());
      swap(storage, next);
    }

    template <class... Args>
    reference emplace_back(Args&&... args)
    {
      // when there is no enough memory grow by constant factor
      if (capacity() == size()) reserve(capacity() == 0 ? 1 : growing_factor * capacity());
      auto ret = storage.last++;
      new (ret) T(cxx::forward<Args>(args)...);
      return *ret;
    }

    void clear() noexcept
    {
      while (!empty()) { pop_back(); }
    }

  private:
    struct storage_t : Allocator {
      // empty base class optimization
      storage_t() noexcept = default;
      storage_t(size_type s) : first{Allocator::allocate(s)}, end_of_storage{first + s} {}

      iterator first{nullptr};
      iterator last{first};
      iterator end_of_storage{first};
    };
    storage_t storage;
  };

  template <class T, class Allocator>
  bool operator==(dynamic_array<T, Allocator> const& lhs, dynamic_array<T, Allocator> const& rhs)
  {
    if (lhs.size() != rhs.size()) return false;
    auto first = rhs.begin();
    for (auto const& x : lhs)
    {
      if (x != *first++) return false;
    }
    return true;
  }

  template <class T, class Allocator>
  bool operator!=(dynamic_array<T, Allocator> const& lhs, dynamic_array<T, Allocator> const& rhs)
  {
    return !(lhs == rhs);
  }

  template <class T, class Allocator = cxx::allocator<T>>
  auto const reserved = [](cxx::size_t s) -> dynamic_array<T, Allocator> {
    dynamic_array<T, Allocator> da;
    da.reserve(s);
    return da;
  };

  template <class T, class Allocator = cxx::allocator<T>>
  auto const values = [](auto... args) -> dynamic_array<T, Allocator> {
    // initialization of an array is guaranteed to be left -> right...
    using in_order = int[];
    auto da = reserved<T, Allocator>(sizeof...(args));
    (void)in_order{0, (da.emplace_back(args), void(), 0)...};
    return da;
  };
} // namespace cxx
