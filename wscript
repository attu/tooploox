#!/usr/bin/env python3
# encoding: utf-8

from waflib.Tools import waf_unit_test
from waflib.Build import BuildContext
import os

with open('VERSION') as f:
    VERSION = f.read().strip()
APPNAME = 'median'

top = '.'
out = 'build'


def init(ctx):
    ctx.load('build_logs')


def options(ctx):
    ctx.load('compiler_cxx')
    ctx.load('gnu_dirs')
    ctx.load('waf_unit_test')
    ctx.add_option('--skip-tests', action='store_true', default=False,
                   help='dont build test binaries')


def configure(ctx):
    ctx.env.DEFINES = os.environ.get('DEFINES', '').split()
    ctx.load('compiler_cxx')
    ctx.load('gnu_dirs')
    ctx.load('clang_compilation_database')
    ctx.load('waf_unit_test')
    ctx.check_cfg(
        package='benchmark',
        args=[
            '--cflags',
            '--libs'],
        mandatory=False)
    ctx.msg('VERSION', VERSION)
    ctx.msg('Used CXXFLAGS', ' '.join(ctx.env.CXXFLAGS))
    ctx.msg('Used LINKFLAGS', ' '.join(ctx.env.LINKFLAGS))
    ctx.msg('Used DEFINES', ' '.join(ctx.env.DEFINES))
    ctx.env.INCLUDES = [ctx.path.abspath(), './includes']


def srclib(f):
    from functools import wraps
    @wraps(f)
    def wrapper(ctx):
        ctx.objects(
            source=ctx.path.ant_glob(['src/lib/**/*.(c|cpp)']),
            target='SRCLIB',
            features='cxx',
        )
        return f(ctx)
    return wrapper


@srclib
def build(ctx):
    for suite in ctx.path.ant_glob(
            ['src/bin/**/*.(c|cpp)']):
        target, _ = suite.name.split('.')
        ctx(
            source=suite,
            target=target,
            vnum=VERSION,
            features='cxx cxxprogram',
            use=['SRCLIB'],
        )

    if not ctx.options.skip_tests:
        ctx(
            source=ctx.path.ant_glob(['test/test_main.cpp']),
            target='test_main',
            features='cxx',
            includes=['test'],
        )

        for suite in ctx.path.ant_glob(
            ['test/**/*.(c|cpp)'],
                excl=['test/test_main.cpp']):
            target, _ = suite.name.split('.')
            ctx(
                source=suite,
                target='ut_' + target,
                features='cxx cxxprogram test',
                use=['test_main', 'SRCLIB'],
                lib=['pthread'],
                install_path=None,
            )

        ctx.add_post_fun(waf_unit_test.summary)
        ctx.add_post_fun(waf_unit_test.set_exit_code)


@srclib
def benchmark(ctx):
    for suite in ctx.path.ant_glob(
            ['benchmark/**/*.(c|cpp)']):
        target, _ = suite.name.split('.')
        ctx(
            source=suite,
            target='benchmark_' + target,
            features='cxx cxxprogram test',
            use=['SRCLIB', 'BENCHMARK'],
            lib=['pthread', 'benchmark_main'],
            install_path=None
        )

    def summary(bld):
        from waflib import Logs
        lst = getattr(bld, 'utest_results', [])
        for(f, code, out, err)in (lst or []):
            Logs.pprint('CYAN', '<<=== %s ===>>' % f)
            out = out.decode('utf-8')
            Logs.pprint('CYAN', str(out))
            Logs.pprint('CYAN', '')

    ctx.add_post_fun(summary)
    ctx.add_post_fun(waf_unit_test.set_exit_code)


@srclib
def fuzzer(ctx):
    for suite in ctx.path.ant_glob(
            ['fuzzer/**/*.(c|cpp)']):
        target, _ = suite.name.split('.')
        ctx(
            source=suite,
            target='fuzzer_' + target,
            features='cxx cxxprogram',
            use=['SRCLIB'],
            install_path=None
        )


class Benchmark(BuildContext):
    cmd = 'benchmark'
    fun = 'benchmark'


class Fuzzer(BuildContext):
    cmd = 'fuzzer'
    fun = 'fuzzer'
