from invoke import task, call
from .feature import test
from .common import merge_flags
from .feature import test as bdd


@task
def configure(ctx, build='debug', prefix='usr', env=dict()):
    env['CXXFLAGS'] = merge_flags(ctx, build, name='cxxflags', env=env)
    env['LINKFLAGS'] = merge_flags(ctx, build, name='linkflags', env=env)
    env['DEFINES'] = merge_flags(ctx, build, name='defines', env=env)
    if 'CXX' not in env:
        env['CXX'] = ctx.build.cxx
    ctx.run(f'./waf configure --prefix={prefix}', env=env)


@task
def build(
        ctx,
        alltests=False,
        notests=False,
        skip_tests=False,
        testcmd=None,
        sanitize=False,
        env=dict()):
    assert not (alltests and notests)
    assert not (alltests and skip_tests)
    alltests = '--alltests' if alltests else ''
    notests = '--notests' if notests else ''
    skip_tests = '--skip-tests' if skip_tests else ''
    testcmd = f'--testcmd={testcmd!r}' if testcmd is not None else ''
    if sanitize:
        env.update(ctx.build.sanitize.env)
    cmd = ' '.join(
        f'./waf build {alltests} {notests} {skip_tests} {testcmd}'.split())
    ctx.run(cmd.strip(), env=env)


bld = build


@task
def install(ctx, destdir, skip_tests=True, env=dict()):
    skip_tests = '--skip-tests' if skip_tests else ''
    cmd = f'./waf install {skip_tests} --destdir={destdir}'
    ctx.run(cmd, env=env)


@task(
    pre=[
        call(
            build,
            alltests=True,
            testcmd='valgrind --error-exitcode=1 %s')])
def valgrind(ctx):
    pass


@task(
    post=[
        call(
            build, alltests=True, sanitize=True), call(
                bdd, sanitize=True)])
def sanitize(ctx, build='debug', env=dict()):
    env['CXXFLAGS'] = ' '.join(ctx.build.sanitize.cxxflags)
    env['LINKFLAGS'] = ' '.join(ctx.build.sanitize.linkflags)
    configure(ctx, build=build, env=env)
    bld(ctx, alltests=True, sanitize=True, env=env)


@task(pre=[call(configure, build='coverage'), build, test])
def coverage(ctx, build_dir='build'):
    cmd = f"gcovr --root={build_dir} --filter='.*cxx.*' --filter='.*src.*'"
    ctx.run(f"{cmd} --fail-under-line=100")
    ctx.run(f"{cmd} --branch --print-summary")
