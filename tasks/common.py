import os


def merge_flags(ctx, build: str, name: str, env: dict = dict()):
    try:
        flags = ctx.build.common[name]
    except KeyError:
        flags = list()
    try:
        flags.extend(ctx.build[build][name])
    except KeyError:
        pass
    flags.extend(env.get(name, '').split())
    flags.extend(env.get(name.upper(), '').split())
    return ' '.join(sorted(set(flags)))


def isexe(fpath):
    return os.path.isfile(fpath) and os.access(fpath, os.X_OK)
