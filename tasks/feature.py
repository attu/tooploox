from invoke import task


@task
def test(ctx, features='features', sanitize=False, env=dict()):
    if sanitize:
        env.update(ctx.build.sanitize.env)
    ctx.run(f'behave -i {features}', env=env)
