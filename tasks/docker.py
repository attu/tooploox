from invoke import task

builder = 'tlbuilder'
runner = 'tlrunner'


@task
def build(ctx, container=builder):
    ctx.run(f'docker-compose build {container}')


@task
def push(ctx, container=builder):
    ctx.run(f'docker-compose push {container}')


@task
def up(ctx, container=runner):
    ctx.run(f'docker-compose up -d {container}')
