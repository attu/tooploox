from invoke import Collection
from . import style
from . import docker
from . import build
from . import benchmark
from . import feature
from . import fuzz


ns = Collection(build.configure, build.build, build.install,
                build.coverage, build.valgrind, build.sanitize)
ns.add_collection(Collection(feature.test), name='feature')
ns.add_collection(Collection(style.format), name='style')
ns.add_collection(Collection(
    benchmark.build,
    benchmark.run), name='benchmark')
ns.add_collection(Collection(
    fuzz.build,
    fuzz.run), name='fuzzer')
ns.add_collection(Collection(
    docker.build,
    docker.push,
    docker.up
), name='docker')
