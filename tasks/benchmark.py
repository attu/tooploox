from invoke import task
from glob import glob


@task
def build(ctx):
    ctx.run('./waf benchmark --notests')


@task
def run(ctx):
    for filename in glob('**/benchmark_*', recursive=True):
        ctx.run(f'{filename}')
