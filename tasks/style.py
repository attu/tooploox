from invoke import task


@task
def format(ctx):
    from glob import glob
    for filename in glob('**/*.[hc]pp', recursive=True):
        if not filename.endswith('catch.hpp'):
            ctx.run(f'clang-format -i {filename}')
