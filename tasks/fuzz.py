from invoke import task
from .common import isexe
from glob import glob
from .build import configure


@task
def build(ctx, build='release', env=dict()):
    env['CXXFLAGS'] = ' '.join(ctx.build.fuzzer.cxxflags)
    env['LINKFLAGS'] = ' '.join(ctx.build.fuzzer.linkflags)
    env['CXX'] = 'clang++'
    configure(ctx, build=build, env=env)
    ctx.run('./waf fuzzer --notests')


@task
def run(ctx, build='release', runs=10000, env=dict()):
    env.update(ctx.build.fuzzer.env)
    for crash in glob('**/crash-*', recursive=True):
        *dir, test, filename = crash.split('/')
        test = next(f for f in glob(
            f'**/fuzzer_{test}', recursive=True) if isexe(f))
        ctx.run(f'{test!r} {crash}', env=env)
    for filename in (
        f for f in glob(
            f'**/fuzzer_*',
            recursive=True) if isexe(f)):
        ctx.run(f'{filename} -runs={runs:d}', env=env)
