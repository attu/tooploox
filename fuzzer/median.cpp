#include "src/lib/median.hpp"
#include <cstddef>
#include <cstdint>
#include <cstdlib>

extern "C" int LLVMFuzzerTestOneInput(const uint8_t* data, size_t size)
{
  constexpr auto const max_size = 1 << 16;
  int32_t const* first = reinterpret_cast<int32_t const*>(data);
  size /= (sizeof(int32_t) / sizeof(uint8_t));
  cxx::median median(static_cast<int>(size));
  while (size > 0)
  {
    auto dist = (size > max_size) ? max_size : size;
    auto last = first + dist;
    while (first != last) { median.push(*first++); }
    size -= dist;
  }
  return 0;
}
