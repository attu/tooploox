#include "cxx/heap.hpp"
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <vector>
#include <algorithm>

extern "C" int LLVMFuzzerTestOneInput(const uint8_t* data, size_t size)
{
  constexpr auto const max_size = 1 << 16;
  int32_t const* first = reinterpret_cast<int32_t const*>(data);
  size /= (sizeof(int32_t) / sizeof(uint8_t));
  std::vector<int> v;
  v.reserve(max_size);
  while (size > 0)
  {
    auto dist = (size > max_size) ? max_size : size;
    auto last = first + dist;
    v.assign(first, last);
    std::make_heap(std::begin(v), std::end(v), std::less<>());
    while (!std::empty(v))
    {
      cxx::pop_heap(std::begin(v), std::end(v), std::less<>());
      if (v.back() != *std::max_element(std::begin(v), std::end(v))) std::abort();
      v.pop_back();
      if (!std::is_heap(std::begin(v), std::end(v), std::less<>())) std::abort();
    }
    size -= dist;
  }
  return 0;
}
