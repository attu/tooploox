#include <benchmark/benchmark.h>
#include "cxx/heap.hpp"
#include <random>
#include <vector>

namespace
{
  auto const max_range = 1 << 16;
  auto const make_random_array = [](long n, std::size_t a = 0) -> std::vector<int> {
    static std::random_device rd;
    static std::mt19937 gen(rd());
    static std::uniform_int_distribution<> dis(0);
    std::vector<int> v;
    v.reserve(static_cast<std::size_t>(n) + a);
    while (n-- > 0) { v.emplace_back(dis(gen)); }
    return v;
  };
} // namespace

static void make_heap(benchmark::State& state)
{
  auto const orig = make_random_array(state.range(0));
  for (auto _ : state)
  {
    state.PauseTiming();
    auto array = orig;
    state.ResumeTiming();
    cxx::make_heap(std::begin(array), std::end(array), std::less<>());
    benchmark::DoNotOptimize(array);
  }
  state.SetComplexityN(state.range(0));
}

static void push_heap(benchmark::State& state)
{
  auto const orig = [&state] {
    auto x = make_random_array(state.range(0), 1);
    cxx::make_heap(std::begin(x), std::end(x), std::less<>());
    return x;
  }();

  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> dis(0);

  for (auto _ : state)
  {
    state.PauseTiming();
    auto array = orig;
    array.emplace_back(dis(gen));
    state.ResumeTiming();
    cxx::push_heap(std::begin(array), std::end(array), std::less<>());
    benchmark::DoNotOptimize(array);
  }
  state.SetComplexityN(state.range(0));
}

static void pop_heap(benchmark::State& state)
{
  auto const orig = [&state] {
    auto x = make_random_array(state.range(0), 1);
    cxx::make_heap(std::begin(x), std::end(x), std::less<>());
    return x;
  }();

  for (auto _ : state)
  {
    state.PauseTiming();
    auto array = orig;
    state.ResumeTiming();
    cxx::pop_heap(std::begin(array), std::end(array), std::less<>());
    benchmark::DoNotOptimize(array);
  }
  state.SetComplexityN(state.range(0));
}

BENCHMARK(make_heap)->RangeMultiplier(8)->Range(2, max_range)->Complexity();
BENCHMARK(push_heap)->RangeMultiplier(8)->Range(2, max_range)->Complexity();
BENCHMARK(pop_heap)->RangeMultiplier(8)->Range(2, max_range)->Complexity();
