#include <benchmark/benchmark.h>
#include "cxx/dynamic_array.hpp"
#include <random>

namespace
{
  auto const max_range = 1 << 16;
  auto const make_random_array = [](long n) -> cxx::dynamic_array<int> {
    static std::random_device rd;
    static std::mt19937 gen(rd());
    static std::uniform_int_distribution<> dis(0);
    auto da = cxx::reserved<int>(static_cast<std::size_t>(n));
    while (n-- > 0) { da.emplace_back(dis(gen)); }
    return da;
  };
} // namespace

static void dynamic_array_default_ctor(benchmark::State& state)
{
  for (auto _ : state) benchmark::DoNotOptimize(cxx::dynamic_array<int>{});
}

static void dynamic_array_empty_reserve(benchmark::State& state)
{
  auto const capa = static_cast<std::size_t>(state.range(0));
  for (auto _ : state) benchmark::DoNotOptimize(cxx::reserved<int>(capa));
  state.SetComplexityN(state.range(0));
}

static void dynamic_array_copy_ctor(benchmark::State& state)
{
  auto const size = state.range(0);
  auto const orig = make_random_array(size);
  for (auto _ : state) benchmark::DoNotOptimize(cxx::dynamic_array<int>{orig});
  state.SetComplexityN(state.range(0));
}

static void dynamic_array_traversing(benchmark::State& state)
{
  auto const size = state.range(0);
  auto const orig = make_random_array(size);
  for (auto _ : state)
  {
    for (auto const& x : orig) benchmark::DoNotOptimize(x);
  }
  state.SetComplexityN(state.range(0));
}

static void dynamic_array_growing(benchmark::State& state)
{
  auto const size = state.range(0);
  auto const orig = make_random_array(size);
  for (auto _ : state)
  {
    auto da = cxx::reserved<int>(0);
    for (auto const& x : orig) benchmark::DoNotOptimize(da.emplace_back(x));
  }
  state.SetComplexityN(state.range(0));
}

static void dynamic_array_clearing(benchmark::State& state)
{
  auto const size = state.range(0);
  auto const orig = make_random_array(size);
  for (auto _ : state)
  {
    state.PauseTiming();
    auto copy = orig;
    state.ResumeTiming();
    copy.clear();
    benchmark::DoNotOptimize(copy.empty());
  }
  state.SetComplexityN(state.range(0));
}

BENCHMARK(dynamic_array_default_ctor);
BENCHMARK(dynamic_array_empty_reserve)->RangeMultiplier(8)->Range(2, max_range)->Complexity();
BENCHMARK(dynamic_array_copy_ctor)->RangeMultiplier(8)->Range(2, max_range)->Complexity();
BENCHMARK(dynamic_array_traversing)->RangeMultiplier(8)->Range(2, max_range)->Complexity();
BENCHMARK(dynamic_array_growing)->RangeMultiplier(8)->Range(2, max_range)->Complexity();
BENCHMARK(dynamic_array_clearing)->RangeMultiplier(8)->Range(2, max_range)->Complexity();
