#include <benchmark/benchmark.h>
#include "src/lib/median.hpp"
#include <random>
#include <vector>

namespace
{
  auto const max_range = 1 << 16;
  auto const make_random_array = [](std::size_t n, std::size_t a = 0) -> std::vector<int> {
    static std::random_device rd;
    static std::mt19937 gen(rd());
    static std::uniform_int_distribution<> dis(0);
    std::vector<int> v;
    v.reserve(n + a);
    while (n-- > 0) { v.emplace_back(dis(gen)); }
    return v;
  };
} // namespace

static void count_median_stream(benchmark::State& state)
{
  auto const data = make_random_array(static_cast<std::size_t>(state.range(0)));
  cxx::median median(data.back());
  for (auto _ : state)
  {
    for (auto x : data) { benchmark::DoNotOptimize(median.push(x)); }
  }
  state.SetComplexityN(state.range(0));
}

BENCHMARK(count_median_stream)->RangeMultiplier(4)->Range(2, max_range)->Complexity();
