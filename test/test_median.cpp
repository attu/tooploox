#include "src/lib/median.hpp"
#include "catch.hpp"
#include <algorithm>

TEST_CASE("initialized median returns first value")
{
  cxx::median median(42);
  REQUIRE(median() == Approx(42.0));
}

TEST_CASE("median holding an even number of values return average")
{
  cxx::median median(42);
  REQUIRE(median.push(43) == Approx(42.5));
}

TEST_CASE("median holding an odd number of values return middle value")
{
  cxx::median median(42);
  REQUIRE(median.push(43) == Approx(42.5));
  REQUIRE(median.push(41) == Approx(42));
  REQUIRE(median.push(44) == Approx(42.5));
  REQUIRE(median.push(44) == Approx(43));
  REQUIRE(median.push(45) == Approx(43.5));
  REQUIRE(median.push(40) == Approx(43));
  REQUIRE(median.push(40) == Approx(42.5));
}
