#include "cxx/utility.hpp"
#include <vector>
#include <type_traits>
#include "test/catch.hpp"

template <class Iterator>
constexpr bool compare_iterator_traits_value =
    std::is_same_v<typename std::iterator_traits<Iterator>::value_type,
                   typename cxx::iterator_traits<Iterator>::value_type>;

template <class Iterator>
constexpr bool compare_iterator_traits_difference =
    std::is_same_v<typename std::iterator_traits<Iterator>::difference_type,
                   typename cxx::iterator_traits<Iterator>::difference_type>;

template <class Iterator>
constexpr bool compare_iterator_traits =
    compare_iterator_traits_value<Iterator>&& compare_iterator_traits_difference<Iterator>;

static_assert(compare_iterator_traits<int*>);
static_assert(compare_iterator_traits<int const*>);
static_assert(compare_iterator_traits<std::vector<int>::const_iterator>);
static_assert(compare_iterator_traits<std::vector<int>::const_iterator>);

TEST_CASE("cxx comparators work correctly")
{
  cxx::less<> const less;
  cxx::greater<> const greater;

  REQUIRE(less(1, 2));
  REQUIRE(!less(1, 1));
  REQUIRE(!less(2, 1));

  REQUIRE(greater(2, 1));
  REQUIRE(!greater(2, 2));
  REQUIRE(!greater(1, 2));
}
