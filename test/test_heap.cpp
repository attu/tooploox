#include "cxx/heap.hpp"
#include "catch.hpp"
#include <vector>
#include <algorithm>

TEST_CASE("can make_heap from vector")
{
  SECTION("empty vector")
  {
    std::vector<int> v;
    REQUIRE(std::empty(v));
    cxx::make_heap(std::begin(v), std::end(v), std::less<>());
    REQUIRE(std::empty(v));
  }
  SECTION("single element")
  {
    std::vector v = {42};
    REQUIRE(std::size(v) == 1);
    cxx::make_heap(std::begin(v), std::end(v), std::less<>());
    REQUIRE(v == std::vector<int>{42});
  }
  SECTION("filled vector")
  {
    auto const check_vector = [](std::vector<int> v) {
      do
      {
        auto data = v;
        cxx::make_heap(std::begin(data), std::end(data), std::less<>());
        REQUIRE(std::is_heap(std::begin(data), std::end(data), std::less<>()));
      } while (std::next_permutation(std::begin(v), std::end(v)));
    };
    check_vector({2, 3});
    check_vector({2, 3, 5});
    check_vector({2, 3, 5, 7});
    check_vector({2, 3, 5, 7, 11});
    check_vector({2, 2, 3, 5, 7});
    check_vector({2, 2, 2, 2, 2});
  }
}

TEST_CASE("can push_heap a vector")
{
  auto const check_vector = [](std::vector<int> v) {
    do
    {
      std::vector<int> data;
      for (auto const x : v)
      {
        data.push_back(x);
        cxx::push_heap(std::begin(data), std::end(data), std::less<>());
        REQUIRE(std::is_heap(std::begin(data), std::end(data), std::less<>()));
      }
    } while (std::next_permutation(std::begin(v), std::end(v)));
  };
  check_vector({2, 3});
  check_vector({2, 3, 5});
  check_vector({2, 3, 5, 7});
  check_vector({2, 3, 5, 7, 11});
  check_vector({2, 2, 3, 5, 7});
  check_vector({2, 2, 2, 2, 2});
}

TEST_CASE("can pop_heap a vector")
{
  auto const check_vector = [](std::vector<int> v) {
    std::make_heap(std::begin(v), std::end(v), std::less<>());
    while (!std::empty(v))
    {
      cxx::pop_heap(std::begin(v), std::end(v), std::less<>());
      REQUIRE(v.back() == *std::max_element(std::begin(v), std::end(v)));
      v.pop_back();
      REQUIRE(std::is_heap(std::begin(v), std::end(v), std::less<>()));
    }
  };
  check_vector({2, 3});
  check_vector({2, 3, 5});
  check_vector({2, 3, 5, 7});
  check_vector({2, 3, 5, 7, 11});
  check_vector({2, 2, 3, 5, 7});
  check_vector({2, 2, 2, 2, 2});
}
