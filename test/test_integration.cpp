#include "cxx/heap.hpp"
#include "cxx/dynamic_array.hpp"
#include "src/lib/heap.hpp"
#include "catch.hpp"
#include <vector>
#include <algorithm>

TEST_CASE("can make_heap from dynamic_array")
{
  SECTION("empty dynamic_array")
  {
    cxx::dynamic_array<int> v;
    REQUIRE(std::empty(v));
    cxx::make_heap(std::begin(v), std::end(v), std::less<>());
    REQUIRE(std::empty(v));
  }
  SECTION("single element")
  {
    auto v = cxx::values<int>(42);
    REQUIRE(std::size(v) == 1);
    cxx::make_heap(std::begin(v), std::end(v), std::less<>());
    REQUIRE(v == cxx::values<int>(42));
  }
  SECTION("filled vector")
  {
    auto const check_vector = [](cxx::dynamic_array<int> v) {
      do
      {
        auto data = v;
        cxx::make_heap(std::begin(data), std::end(data), std::less<>());
        REQUIRE(std::is_heap(std::begin(data), std::end(data), std::less<>()));
      } while (std::next_permutation(std::begin(v), std::end(v)));
    };
    check_vector(cxx::values<int>(2, 3));
    check_vector(cxx::values<int>(2, 3, 5));
    check_vector(cxx::values<int>(2, 3, 5, 7));
    check_vector(cxx::values<int>(2, 3, 5, 7, 11));
    check_vector(cxx::values<int>(2, 2, 3, 5, 7));
    check_vector(cxx::values<int>(2, 2, 2, 2, 2));
  }
}

TEST_CASE("can push_heap a vector")
{
  auto const check_vector = [](cxx::dynamic_array<int> v) {
    do
    {
      cxx::dynamic_array<int> data;
      for (auto x : v)
      {
        data.emplace_back(x);
        cxx::push_heap(std::begin(data), std::end(data), std::less<>());
        REQUIRE(std::is_heap(std::begin(data), std::end(data), std::less<>()));
      }
    } while (std::next_permutation(std::begin(v), std::end(v)));
  };
  check_vector(cxx::values<int>(2, 3));
  check_vector(cxx::values<int>(2, 3, 5));
  check_vector(cxx::values<int>(2, 3, 5, 7));
  check_vector(cxx::values<int>(2, 3, 5, 7, 11));
  check_vector(cxx::values<int>(2, 2, 3, 5, 7));
  check_vector(cxx::values<int>(2, 2, 2, 2, 2));
}

TEST_CASE("can pop_heap a vector")
{
  auto const check_vector = [](cxx::dynamic_array<int> v) {
    std::make_heap(std::begin(v), std::end(v), std::less<>());
    while (!std::empty(v))
    {
      cxx::pop_heap(std::begin(v), std::end(v), std::less<>());
      REQUIRE(v.back() == *std::max_element(std::begin(v), std::end(v)));
      v.pop_back();
      REQUIRE(std::is_heap(std::begin(v), std::end(v), std::less<>()));
    }
  };
  check_vector(cxx::values<int>(2, 3));
  check_vector(cxx::values<int>(2, 3, 5));
  check_vector(cxx::values<int>(2, 3, 5, 7));
  check_vector(cxx::values<int>(2, 3, 5, 7, 11));
  check_vector(cxx::values<int>(2, 2, 3, 5, 7));
  check_vector(cxx::values<int>(2, 2, 2, 2, 2));
}

TEST_CASE("can create empty heap")
{
  cxx::heap<int> heap;
  REQUIRE(std::empty(heap));
}

TEST_CASE("heap can add and remove items")
{
  auto const check_vector = [](cxx::dynamic_array<int> v) {
    do
    {
      auto data = cxx::reserved<int>(v.size());
      cxx::heap<int> heap;
      for (auto x : v)
      {
        data.emplace_back(x);
        heap.push(x);
        REQUIRE(!std::empty(heap));
        REQUIRE(std::size(heap) == std::size(data));
        REQUIRE(heap.front() == *std::max_element(std::begin(data), std::end(data)));
        REQUIRE(std::is_heap(std::begin(heap), std::end(heap), heap.get_compare()));
      }
      while (!std::empty(heap))
      {
        auto top = heap.front();
        REQUIRE(heap.pop() == top);
        REQUIRE(std::is_heap(std::begin(heap), std::end(heap), heap.get_compare()));
      }
    } while (std::next_permutation(std::begin(v), std::end(v)));
  };
  check_vector(cxx::values<int>(2, 3, 5, 5, 7, 11));
}
