#include "cxx/dynamic_array.hpp"
#include "catch.hpp"
#include <algorithm>
#include <memory>

TEST_CASE("can create empty dynamic array")
{
  cxx::dynamic_array<int> const da;
  REQUIRE(std::size(da) == 0);
  REQUIRE(std::empty(da));
  REQUIRE(da.capacity() == 0);
}

TEST_CASE("dynamic array can reserve memory")
{
  auto const da = cxx::reserved<int>(42);
  REQUIRE(std::size(da) == 0);
  REQUIRE(std::empty(da));
  REQUIRE(da.capacity() >= 42);
}

TEST_CASE("can emplace_back item into reserved dynamic array")
{
  auto da = cxx::reserved<int>(42);
  REQUIRE(std::empty(da));
  REQUIRE(da.capacity() > 0);
  auto const capa = da.capacity();
  auto const first = std::begin(da);
  auto const& item = da.emplace_back(7);
  REQUIRE(capa == da.capacity());
  REQUIRE(first == std::begin(da));
  REQUIRE(!std::empty(da));
  REQUIRE(std::size(da) == 1);
  REQUIRE(item == 7);
}

TEST_CASE("can emplace_back item into unreserved dynamic array")
{
  auto da = cxx::reserved<int>(0);
  REQUIRE(std::empty(da));
  REQUIRE(da.capacity() == 0);
  auto const& item = da.emplace_back(7);
  REQUIRE(da.capacity() != 0);
  REQUIRE(!std::empty(da));
  REQUIRE(std::size(da) == 1);
  REQUIRE(item == 7);
}

TEST_CASE("can emplace_back item into non-empty dynamic array")
{
  auto da = cxx::reserved<int>(3);
  da.emplace_back(2);
  da.emplace_back(3);
  da.emplace_back(5);
  REQUIRE(!std::empty(da));
  REQUIRE(da.capacity() == 3);
  auto const first = std::begin(da);

  auto const& item = da.emplace_back(7);
  REQUIRE(da.capacity() > 0);
  REQUIRE(!std::empty(da));
  REQUIRE(std::size(da) == 4);
  REQUIRE(first != std::begin(da));
  REQUIRE(item == 7);
}

TEST_CASE("can clear empty dynamic array")
{
  auto da = cxx::reserved<int>(0);
  REQUIRE(std::empty(da));
  da.clear();
  REQUIRE(std::empty(da));
}

TEST_CASE("can clear non-empty dynamic array")
{
  auto da = cxx::reserved<int>(3);
  da.emplace_back(2);
  da.emplace_back(3);
  da.emplace_back(5);
  REQUIRE(!std::empty(da));
  da.clear();
  REQUIRE(std::empty(da));
}

TEST_CASE("can copy construct dynamic array")
{
  auto const orig = cxx::values<int>(2, 3, 5, 7, 11);
  auto const copy = orig;

  REQUIRE(std::size(copy) == std::size(orig));
  REQUIRE(copy.capacity() >= std::size(orig));
  REQUIRE(std::begin(copy) != std::begin(orig));
  REQUIRE(std::equal(std::begin(orig), std::end(orig), std::begin(copy)));
}

TEST_CASE("can copy assign dynamic array")
{
  auto const orig = cxx::values<int>(2, 3, 5, 7, 11);
  auto copy = cxx::values<int>(13, 17, 19);
  REQUIRE(std::size(copy) != std::size(orig));

  copy = orig;
  REQUIRE(std::size(copy) == std::size(orig));
  REQUIRE(copy.capacity() >= std::size(orig));
  REQUIRE(std::begin(copy) != std::begin(orig));
  REQUIRE(std::equal(std::begin(orig), std::end(orig), std::begin(copy)));
}

TEST_CASE("can move construct dynamic array")
{
  auto const orig = cxx::values<int>(2, 3, 5, 7, 11);
  auto moved = orig;
  cxx::dynamic_array<int> copy = std::move(moved);

  REQUIRE(std::size(moved) == 0);
  REQUIRE(moved.capacity() == 0);
  REQUIRE(std::size(copy) == std::size(orig));
  REQUIRE(copy.capacity() >= std::size(orig));
  REQUIRE(std::begin(copy) != std::begin(orig));
  REQUIRE(std::equal(std::begin(orig), std::end(orig), std::begin(copy)));
}

TEST_CASE("can move assign dynamic array")
{
  auto const orig = cxx::values<int>(2, 3, 5, 7, 11);
  auto moved = orig;
  auto const ref = cxx::values<int>(13, 17, 19);
  auto assigned = ref;
  REQUIRE(std::size(assigned) != std::size(orig));
  REQUIRE(!std::empty(assigned));

  assigned = std::move(moved);
  REQUIRE(std::size(assigned) == std::size(orig));
  REQUIRE(assigned.capacity() >= std::size(orig));
  REQUIRE(std::begin(assigned) != std::begin(orig));
  REQUIRE(std::equal(std::begin(orig), std::end(orig), std::begin(assigned)));

  REQUIRE(std::size(moved) == std::size(ref));
  REQUIRE(moved.capacity() >= std::size(ref));
  REQUIRE(std::begin(moved) != std::begin(ref));
  REQUIRE(std::equal(std::begin(ref), std::end(ref), std::begin(moved)));
}

TEST_CASE("can pop_back item from dynamic array")
{
  auto const orig = cxx::values<int>(2, 3, 5, 7, 11);
  auto da = orig;
  REQUIRE(!std::empty(da));
  auto const size = std::size(da);
  auto const capa = da.capacity();

  da.pop_back();
  REQUIRE(std::size(da) == (size - 1));
  REQUIRE(da.capacity() == capa);
  REQUIRE(std::equal(std::begin(da), std::end(da), std::begin(orig)));
}

TEST_CASE("can clear items from dynamic array")
{
  auto da = cxx::values<int>(2, 3, 5, 7, 11);
  REQUIRE(!std::empty(da));
  auto const capa = da.capacity();

  da.clear();
  REQUIRE(std::empty(da));
  REQUIRE(da.capacity() == capa);
}

TEST_CASE("can access front and back items")
{
  auto orig = cxx::values<int>(2, 3, 5, 7, 11);
  auto const* ptr = &orig;

  REQUIRE(orig.front() == 2);
  REQUIRE(ptr->front() == 2);

  orig.front() = 42;
  REQUIRE(orig.front() == 42);
  REQUIRE(ptr->front() == 42);

  REQUIRE(orig.back() == 11);
  REQUIRE(ptr->back() == 11);

  orig.back() = 666;
  REQUIRE(orig.back() == 666);
}

TEST_CASE("dynamic array can hold move-only resources")
{
  auto da = cxx::reserved<std::unique_ptr<int>>(42);
  auto const ref = cxx::values<int>(2, 3, 5, 7, 11);
  for (auto const x : ref)
  {
    auto& v = da.emplace_back(std::make_unique<int>(x));
    REQUIRE(*v == x);
  }
  REQUIRE(std::size(da) == std::size(ref));

  da.pop_back();
  REQUIRE(std::size(da) == (std::size(ref) - 1));
  da.reserve(da.capacity() + 1);
  REQUIRE(std::size(da) == (std::size(ref) - 1));
  auto const comp = [](auto const& x, auto const& y) { return *x == y; };
  REQUIRE(std::equal(std::begin(da), std::end(da), std::begin(ref), comp));
  da.clear();
  REQUIRE(std::empty(da));
}

TEST_CASE("can compare dynamic_array")
{
  REQUIRE(cxx::values<int>(2, 3, 5) != cxx::values<int>(2, 3));
  REQUIRE(cxx::values<int>(2, 3, 5) != cxx::values<int>(2, 3, 7));
  REQUIRE(cxx::values<int>(2, 3, 5) == cxx::values<int>(2, 3, 5));
}
