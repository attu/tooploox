# Changelog

All notable changes to communication will be documented in this file

---

## [0.1]
### Added
- `stream_median` binary
- CI setup
- Docker images
- UTs
- Benchmarks
- Fuzzy tests
- Behavioral tests
- `cxx::dynamic_array`
- `cxx::make_heap`, `cxx::push_heap`, `cxx::pop_heap`
- documentation

### Changed
### Fixed
### Removed
